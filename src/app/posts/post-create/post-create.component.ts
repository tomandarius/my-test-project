import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostsService } from '../post.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  maxDate = new Date();
  form: FormGroup;

  constructor(private postService: PostsService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(4)],
      }),
      content: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.maxLength(140)]
      }),
      date: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      })
    });

  }

  onAddPost() {
    if (this.form.invalid) {
      return console.log('Form is invalid!');
    }
    this.postService.addPost(
      this.form.value.title,
      this.form.value.content,
      this.form.value.date);
    this.form.reset();
    Object.keys(this.form.controls).forEach(key => {
      this.form.controls[key].setErrors(null);
    });
    this.snackBar.open('Your post was save!', 'Close', {
      duration: 2000,
    });
  }
}

