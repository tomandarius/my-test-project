import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../post.model';
import { PostsService } from '../post.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
 posts: Post[] = [];
 private postSub: Subscription;



  constructor(
    private postService: PostsService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.postService.getPosts();
    this.postSub = this.postService.getPostUpdateListener()
      .subscribe((posts: Post[]) => {
        this.posts = posts;
      });
  }

  onDelete(postId: string) {
    this.postService.openConfirmatonDialog()
    .afterClosed()
    .subscribe(res => {
      if (res) {
        this.postService.deletePost(postId);
        this.snackBar.open('Your post was deleted!', 'Close', {
          duration: 2000,
        });
      }
    });
  }

  ngOnDestroy() {
   this.postSub.unsubscribe();
  }
}
